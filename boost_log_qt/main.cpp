#include "logger.h"
#include "test_fcn.h"


int main(int, char*[])
{
    logInit();

    writeLogMsgInfo("info from main test");
    writeLogError("error");
    writeLogMsgWarning("warning");
    return 0;
}
