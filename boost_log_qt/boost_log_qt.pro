QT += core
QT -= gui

TARGET = boost_log_qt
CONFIG += console
CONFIG -= app_bundle

DEFINES += BOOST_LOG_DYN_LINK

TEMPLATE = app

SOURCES += \
    main.cpp \
    logger.cpp

LIBS += -lboost_log -lboost_system -lboost_thread -lboost_log_setup

HEADERS += \
    test_fcn.h \
    logger.h

