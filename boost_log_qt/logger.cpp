#include "logger.h"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
namespace keywords = boost::log::keywords;
namespace sinks = boost::log::sinks;

namespace expr = boost::log::expressions;
BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::posix_time::ptime)
BOOST_LOG_ATTRIBUTE_KEYWORD(line_id, "LineID", unsigned int)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", logging::trivial::severity_level)



void logInit()
{

    logging::add_common_attributes();
    //boost::shared_ptr<logging::core> core = logging::core::get();
    boost::log::register_simple_formatter_factory< boost::log::trivial::severity_level, char >("Severity");
    logging::add_file_log(keywords::file_name = "log_file_%N.log",
                          keywords::rotation_size = 10 * 1024 * 1024,
                          keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
                          keywords::auto_flush = true,
                          keywords::open_mode = std::ios_base::app,
                          keywords::format = (
                expr::stream <<  line_id << " " << expr::format_date_time(timestamp, "%Y-%m-%d %H:%M:%S")
                << " [" << logging::trivial::severity << "] " << expr::smessage));

    logging::core::get()->set_filter
            (
                logging::trivial::severity >= logging::trivial::info
                );
#define USE_LOGGER true
}

void writeLogMsgInfo(const std::string &msg)
{
    if(USE_LOGGER)
        BOOST_LOG_TRIVIAL(info) << msg;
}

void writeLogError(const std::string &msg)
{
    if(USE_LOGGER)
        BOOST_LOG_TRIVIAL(error) << msg;
}

void writeLogMsgWarning(const std::string &msg)
{
    if(USE_LOGGER)
        BOOST_LOG_TRIVIAL(warning) << msg;
}
